@AUTHOR: Felipe Flamarion da Conceição
@DATA: 2016-08-09

@Instituto Federal Catarinense - Campus Araquari

Projeto da disciplina - Desenvolvimento Web II - Professor Marco André Lopes Mendes

Projeto ativo em: http://flamarion.pythonanywhere.com/

===================================================================================
#OBJETIVOS

> Organizar Models, Forms, Views e Admin em arquivos separados
... Feito

> Utilizar Class Based Views (CBVs)
... Feito

> Utilizar outro framework CSS diferente do Bootstrap
... Feito (Semantic)

> Fazer a Localização (tradução para outros idiomas) da aplicação
... ----

> Criar a documentação da aplicação com Sphinx (ou semelhante)
... Feito parcialmente

> Utilizar Testes de Unidade na aplicação
... Feito parcialmente

> Implementar pelo menos uma função nova no sistema, que envolva modificação do modelo de dados
... Feito (Comentário nas categorias)

> Uso de uma biblioteca alternativa de login e registro (como a Django-Registration-Redux)
... ----


===================================================================================

- VIRTUALENV, VIRTUALWRAPPER -

#INSTALAÇÃO

Ambiente: Linux, Python2.7.12, pip

#Passo 1: pip install virtualenv virtualenvwrapper

#Passo 2: Add "source virtualenvwrapper.sh" to .bashrc

* diretório "~/.virtualenvs/" será criado
* lsvirtualenv: listar ambientes virtuais

#CRIAÇÃO do ambiente virtual

# virtualenv ~/.virtualenvs/nome_do_ambiente
* "nome_do_ambiente" irá aparecer quando "lsvirtualenv" for chamado

#UTILIZAR ambiente virtual

# workon nome_do_ambiente

- DJANGO - *Com o ambiente virtual ativo*

# pip install django==1.7

==================================================================================
CRIANDO PROJETO *Com o ambiente virtual ativo*

# mkdir ~/projetos
# cd ~/projetos

# django-admin startproject meu_projeto

==================================================================================
SINC COM O GIT-BITBUCKET

# Criar repositório remoto no bitbucket

# mkdir ~/projetos/meu_projeto
# git init
# git remote add origin "caminho_repositorio_bitbucket.git"

Commit inicial
# git commit -m "Commit inicial"
# git push origin master

================
# pip freeze > requirements.txt

# pip install -r requirements.txt
=================
GITIGNORE

==================================================================================
====================================# DJANGO #====================================
==================================================================================

ARQUIVOS:

__init__.py
    -   Arquivo de inicialização. A pasta que contém o "__init__" é considerada um "pacote" python.
settings.py
    -   Configurações do projeto.
urls.py
    -   Mapeamento das urls para as views: "rotas". Ponto de entrada do sistema.
wsgi.pt
    -   Para servidor WSGI.

Runserver: servidor do próprio Django. Simples e limitado. Apenas para a fase de produção do sistema.

=================

SETTINGS