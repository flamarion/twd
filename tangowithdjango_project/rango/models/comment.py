#coding:utf-8
from django.db import models
from rango.models import Category, UserProfile

class Comment(models.Model):

    text = models.CharField(max_length=140)
    moment = models.DateTimeField(auto_now_add=True, blank=True)
    category = models.ForeignKey(Category, default='')
    profile = models.ForeignKey(UserProfile)