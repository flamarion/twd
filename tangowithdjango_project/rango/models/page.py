#coding:utf-8
from django.db import models
from rango.models import Category
from datetime import datetime

class Page(models.Model):

    category = models.ForeignKey(Category)
    title = models.CharField(max_length=128, unique=True)
    url = models.URLField()
    views = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        return self.title