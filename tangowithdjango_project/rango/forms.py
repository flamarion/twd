#coding: utf-8
from django import forms
from models import Page, Category, UserProfile
from django.contrib.auth.models import User

class CategoryForm(forms.ModelForm):

    name = forms.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': 'Web development'
        }),
        error_messages={
            'required': 'This field is required!'
        }
    )

    class Meta:
        model = Category
        fields = ['name']

class PageForm(forms.ModelForm):

    title = forms.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': 'Django project'
        })
    )
    url = forms.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': 'https://www.djangoproject.com/'
        })
    )

    class Meta:
        model = Page
        exclude = ['views']

class UserForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput())
    username = forms.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': 'john.snow'
        }),
    )
    email = forms.CharField(
        widget=forms.EmailInput(attrs={
            'placeholder': 'john.snow@nightswatch.com'
        }),
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class UserProfileForm(forms.ModelForm):

    website = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'http://www.lordcommander.com/'
    }), required=False)

    class Meta:
        model = UserProfile
        fields = ('website', 'picture')
