#coding: utf-8

from django.shortcuts import render
from rango.models import Category, Page
from django.views.generic import View

class Index(View):
    '''Render TWDs Home Page. Show all categories, top categories, recently added pages and top pages'''
    def get(self, request):
        context = {}
        categories = Category.objects.all().order_by('name')
        context["categories"] = categories
        context["top_liked_categories"] = categories.order_by('-likes')[:5]
        context["top_seen_pages"] = Page.objects.all().order_by('-views')[:5]
        context["recent_pages"] = Page.objects.all().order_by('-id')[:5]
        return render(request, 'rango/index.html', context)