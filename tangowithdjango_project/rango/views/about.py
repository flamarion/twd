#coding: utf-8

from django.shortcuts import render
from django.views.generic import View

class About(View):

    def get(self, request):
        context = dict(
            autor='Felipe Flamarion',
            instituicao='Instituto Federal Catarinense - Campus Araquari',
            curso='Bacharelado em Sistemas de Informação (BSI)',
            disciplina='Desenvolvimento Web 2',
            professor='Marco André Lopes Mendes'
        )
        return render(request, 'rango/about.html', context)