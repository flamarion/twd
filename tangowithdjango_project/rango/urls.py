#coding: utf-8
from django.conf.urls import patterns, url, include

from rango.views import *

urlpatterns = patterns('',
    url(r'^$', Index.as_view(), name="index"),
    url(r'^index/', Index.as_view(), name="index"),
    url(r'^about/', About.as_view(), name="about"),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/$', ViewCategory.as_view(), name="view_category"),
    url(r'^like/(?P<category_name_slug>[\w\-]+)/$', LikeCategory.as_view(), name="like_category"),
    url(r'^add_category/$', AddCategory.as_view(), name="add_category"),
    url(r'^add_page/$', AddPage.as_view(), name="add_page"),
    url(r'^categories/', CategoriesList.as_view(), name="categories"),
    url(r'^register/', RegisterUser.as_view(), name="register_user"),
    url(r'^settings/', UserSettings.as_view(), name="user_settings"),
    url(r'^login/', LogIn.as_view(), name="login"),
    url(r'^logout/', LogOut.as_view(), name="logout"),
    url(r'^comment/', SendComment.as_view(), name="comment"),
)