.. tango_with_django documentation master file, created by
   sphinx-quickstart on Tue Dec 13 09:52:30 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tango With Django's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   modules/models


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
