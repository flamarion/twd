TWD'S Installation
==================

1) Create an directory

**$ mkdir directory_name**

**$ cd directory_name/**

2) Clone the project code

**$ git clone https://bitbucket.org/flamarion/twd.git**

3) Install resources

**$ pip install -r requirements.txt**

4.1) Create database

**$ python manage.py makemigrations**

**$ python manage.py migrate**

4.2) Populate the rango database

**$ python populate_rango**

5) Init application

**$ python manage.py runserver**